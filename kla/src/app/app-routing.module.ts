import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/pages/home/home.component';
import { Page1Component } from './components/pages/page1/page1.component';
import { AlerisComponent } from './components/pages/aleris/aleris.component';
import { F1Component } from './components/pages/f1/f1.component';
import { ShelbyComponent } from './components/pages/shelby/shelby.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: '11k', component: Page1Component},
  { path: 'aleris', component: AlerisComponent},
  { path: 'f1', component: F1Component},
  { path: 'shelby', component: ShelbyComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
