import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlerisComponent } from './aleris.component';

describe('AlerisComponent', () => {
  let component: AlerisComponent;
  let fixture: ComponentFixture<AlerisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlerisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlerisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
